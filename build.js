// the node child process handler
var childProcess = require('child_process'),
  // the node filesystem module
  fileSystem = require('fs'),
  // include the underscore module
  _ = require('underscore'),
  // the r.js command-line command
  rJs = 'r.js -o requirejs.build.js',
  // the folder to start scanning in
  outputDirectory = 'build',
  // the list of folders to delete
  deleteFolders = [],
  // the list of loose files to delete that r.js could not remove
  deleteFiles = [
    //outputDirectory + '/app/weather.js'
  ],
  /**
   * Cleans up after r.js has compressed and uglified the js/css/html files
   *
   * @method cleanupBuild
   * @param {Error} aError The error that had occurred if any
   * @param {String} aStdOut The output from the r.js script
   * @param {String} aStdError The error output from the r.js script
   */
  cleanupBuild = function (aError, aStdOut, aStdError) {
    /**
     * Deletes a file from the build
     *
     * @method deleteFile
     * @param {String} aFile The file path to be deleted
     */
    var deleteFile = function (aFile) {
      // delete the file with fs
      fileSystem.unlinkSync(aFile);
    },
      /**
       * Deletes a folder from the build
       *
       * @method deleteFolder
       * @param {String} aFolder The folder path to be deleted
       */
      deleteFolder = function(aFolder) {
        /**
         * Deletes a single file within the folder
         *
         * @method checkFile
         * @param {String} aFile The file to check
         */
        var checkFile = function(aFile){
          // get the name of the file to check
          var aFile = aFolder + "/" + aFile;
          // check if the file is actually a directory
          if(fileSystem.lstatSync(aFile).isDirectory()) {
            // call the delete folder method again, now with this directory
            deleteFolder(aFile);
          }
          else {
            // delete the file
            fileSystem.unlinkSync(aFile);
          }
        };
        // check that the folder exists
        if(fileSystem.existsSync(aFolder)) {
          // read the folder and delete each file
          fileSystem.readdirSync(aFolder).forEach(checkFile);
          // remove this folder
          fileSystem.rmdirSync(aFolder);
        }
      };
    // make sure r.js ran successfully
    if (!aError) {
      // log a success message to show that r.js ran successfully
      console.log('Complete.');
      // add a message that we are now deleting files left over from the build
      console.log('Deleting files left over from the build.');
      // delete all the files that need to be deleted
      _.each(deleteFiles, deleteFile);
      // delete all the folders that need to be deleted
      _.each(deleteFolders, deleteFolder);
      // log that the necessary files have been deleted
      console.log('Complete.');
    }
    else {
      // log an error
      console.log('Failed with output:');
      console.log(aStdOut);
    }
  };

// output that the build has started
console.log('Starting JS/CSS/HTML compression using r.js.');
// execute r.js to kick off the build
childProcess.exec(rJs, {}, cleanupBuild);
