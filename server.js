// inlude the node http module
var http = require('http'),
  // include the filesystem node module
  fs = require('fs'),
  // include the path node module
  path = require('path'),
  // include the underscore module
  _ = require('underscore'),
  // include the server tools
  serverTools = require('./server-tools.js'),

  /**
   * Starts a node server that proxies API data on one port and that serves mock
   * data on a second port.
   *
   * @method nodeServer
   * @param {Object} aServerSettings
   * @param {String} aServerSettings.liveServerHost The host of the live api
   *   server (Without the "http://")
   * @param {Integer} aServerSettings.liveServerPort The port to run the "live"
   *   node server. This server serves files locally and proxies all api calls
   *   to the live server
   * @param {Integer} aServerSettings.mockServerPort The port to run the "mock"
   *   node server. The server serves files locally and proxies all api calls
   *   to a local express mock data server (Also created by this method)
   * @param {Integer} aServerSettings.mockApiServerPort The port to run the
   *   "mock api" node server. This is an express server serving mock data
   */
  nodeServer = function (aServerSettings) {
    // a list of settings for the node server
    var settings = {
      // the live server port
      liveServerPort : 5408
    },
      /**
       * Initialises the node server by setting all the passed-in settings
       *
       * @method initialise
       */
      initialise = function () {
        // extend the settings with the list of passed in settings
        _.extend(settings, aServerSettings);
        // create the live http server
        createHttpServer(settings.liveServerPort);
      },
      /**
       * Creates an http server
       *
       * @method createHttpServer
       * @param {Integer} aPort The port the server must listen on
       */
      createHttpServer = function (aPort) {
        /**
         * Handles a request on the server
         *
         * @method handleRequest
         * @param {Object} aRequest The http request that was received
         * @param {Object} aResponse The http response object
         */
        handleRequest = function (aRequest, aResponse) {
          // the url that was requested
          var requestUrl = aRequest.url,
            // the path to the file
            filePath = requestUrl === '/' ? './index.html' : '.' + requestUrl,
            // the request method
            requestMethod = aRequest.method,
            // the file extension
            fileExtension = path.extname(filePath),
            // the body of the request
            requestBody = '',
            /**
             * Gets the content-type from a file extension
             *
             * @method getContentType
             * @param {String} aFileExtension The extension of the file (ex: ".js")
             * @return {String} The content-type of this type of file
             */
            getContentType = function (aFileExtension) {
              // A list of file extensions with their matching content-types
              var contentTypes = {
                  // javascript files
                  '.js' : 'text/javascript',
                  // stylesheets
                  '.css' : 'text/css',
                  // web-pages
                  '.html' : 'text/html'
                };
              // return the matching content-type
              return contentTypes[aFileExtension || '.html'];
            },
            // the content-type of the file being requested
            contentType = getContentType(fileExtension),
            // whther this is an api call
            apiCall = filePath.indexOf('/api/') > -1,
            // the port the request is being made on
            currentPort = parseInt(aRequest.headers.host.split(':')[1], 10),
            /**
             * Serves a static file from the local machine
             *
             * @method serveStaticFile
             */
            serveStaticFile = function () {
              /**
               * A callback for fs.exists, reactive on whether a local file
               *   exists or not for the requested path
               *
               * @method fileExists
               * @param {Boolean} aExists Whether the file was found
               */
              var fileExists = function (aExists) {
                /**
                 * Callback for fs.readFile. reads a file from local disk
                 *
                 * @method readFile
                 * @param {Boolean} aError If an error had occurred
                 * @param {String} aContent The file contents
                 */
                var readFile = function (aError, aContent) {
                  // see if an error occurred while reading the file
                  if (aError) {
                    // respond with a "500 - Internal server error"
                    serverTools.respond500(aRequest, aResponse);
                  }
                  else {
                    // respond with a "200 - Success"
                    serverTools.respond200(aRequest, aResponse, aContent,
                      contentType);
                  }
                };
                // check if the file exists
                if (aExists) {
                  // read the file using node's filesystem module
                  fs.readFile(filePath, readFile);
                }
                else {
                  // throw a "404 - Not found" error
                  serverTools.respond404(aRequest, aResponse);
                }
              };
              // check if the file exists
              fs.exists(filePath, fileExists);
            };
          // serve the static file
          serveStaticFile();
        };
        // create the node http server
        http.createServer(handleRequest).listen(aPort);
        // log about the server being created
        console.log('New node server listening at http://localhost:' + aPort);
      };

    // auto-initialise this class
    initialise();
  },

  newNodeServer = new nodeServer();
