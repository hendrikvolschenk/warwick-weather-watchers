// the node file system module
var fs = require('fs');
// create the server tools node module
module.exports = {
  /**
   * Respond with a "200 - Success"
   *
   * @method respond200
   * @param {Object} aRequest The http request
   * @param {Object} aResponse The http response object
   * @param {String} aContent The content to respond with
   * @param {String} aContentType The content type of the content
   */
  respond200 : function (aRequest, aResponse, aContent, aContentType) {
    // Respond with a "200 - Success"
    aResponse.writeHead(200, {
        'Content-type' : aContentType || 'application/json'
    });
    // end the response
    aResponse.end(aContent, 'utf-8');
  },
  /**
   * Respond with a "500 - Internal server error"
   *
   * @method respond500
   * @param {Object} aRequest The http request
   * @param {Object} aResponse The http response object
   */
  respond500 : function (aRequest, aResponse) {
    // throw out a "500 - Internal server error"
    aResponse.writeHead(500);
    // end the response
    aResponse.end();
  },
  /**
   * Respond with a "404 - Not found"
   *
   * @method respond404
   * @param {Object} aRequest The http request
   * @param {Object} aResponse The http response object
   */
  respond404 : function (aRequest, aResponse) {
    // throw out a "500 - Internal server error"
    aResponse.writeHead(404);
    // end the response
    aResponse.end();
  },
  /**
   * Respond with any other response code not specified above
   *
   * @method respondOther
   * @param {Object} aRequest The http request
   * @param {Object} aResponse The http response object
   * @param {Integer} aResponseCode The http response code to send
   * @param {String} aContent The content to respond with
   * @param {String} aContentType The content type of the content
   */
  respondOther : function (aRequest, aResponse, aResponseCode, aContent,
    aContentType) {
    // Respond with the given response code
    aResponse.writeHead(aResponseCode, {
        'Content-type' : aContentType || 'application/json'
    });
    // end the response
    aResponse.end(aContent, 'utf-8');
  }
};