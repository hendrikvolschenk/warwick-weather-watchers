Warwick weather watchers.

A simple GeoLocation based weather application that reads weather information
from openweathermap.org

Please have nodejs pre-installed.

To set up, clone the repository and install the required modules:

  $ npm install

A grunt runner is available to build css (from less) and to jslint, the default
task is 'watch'

  $ grunt

To start up the local server (That defaults to port 888) run:

  $ npm start

or

  $ node server.js

You can open your browser to http://localhost:888 where you should see Warwick
weather watchers in action.

If you'd like to build the application to a small uploadable format run:

  $ node build.js

This will output the contents to the /build folder.

If you have this file you probably know the github repository is located at:
https://github.com/righteous-trespasser/warwick-weather-watchers