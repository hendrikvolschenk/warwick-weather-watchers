require(
  [
    'react',
    'reactdom',
    'weather/weather'
  ],
  function (React, ReactDOM, Weather) {
    // add the weather element to the dom
    ReactDOM.render(React.createElement(Weather(), null),
      document.getElementById('main'));

  }
);