// set up RequireJS with the necessary configuration
require.config({
  // the base url where all the files are located
  baseUrl : 'app',
  // the paths to files that are not in the folder structure
  paths : {
    // jQuery
    'jquery' : 'https://cdnjs.cloudflare.com/ajax/libs/jquery/3.1.0/jquery.min',
    // ReactJS
    'react' : 'https://cdnjs.cloudflare.com/ajax/libs/react/15.2.1/react.min',
    // ReactJs DOM
    'reactdom' : 'https://cdnjs.cloudflare.com/ajax/libs/react/15.2.1/react-dom.min'
  }
});

// include the loader which will kick-off the fun
require(['loader']);