define(
  [
    'react'
  ],
  function (React) {

    'use strict';

    /**
     * A method to wrap private methods and return the react class
     */
    return function () {
      /**
       * The weather location react class that will house the location display
       */
      var WeatherLocation = React.createClass({
        // the html tag that will identify this class
        displayName : 'WeatherLocation',
        /**
         * Renders the weather location element
         *
         * @method render
         */
        render : function render() {
          // a heading1 element that will display the short name of the
          // current weather conditions
          return React.createElement('span', null, this.props.locationName);
        }
      });
      // return the weather location react class
      return WeatherLocation;
    };
  }
);