define(
  [
    'react'
  ],
  function (React) {

    'use strict';

    // a reference to react's createElement method
    var CREATE_ELEMENT = React.createElement;

    /**
     * A method to wrap private methods and return the react class
     */
    return function () {
      /**
       * The weather refresh react class that will house the weather display
       */
      var WeatherRefresh = React.createClass({
        // the html tag that will identify this class
        displayName : 'WeatherRefresh',
        /**
         * Renders the weather refresh element
         *
         * @method render
         */
        render : function render() {
          // the list of properties
          var properties = this.props;
          // a heading1 element that will display the short name of the
          // current weather conditions
          return CREATE_ELEMENT('div',
            {className : 'col-xs-12 text-right refresh'}, CREATE_ELEMENT('a',
              {onClick : this.props.refresh}, 'Refresh'));
        }
      });
      // return the weather refresh react class
      return WeatherRefresh;
    };
  }
);