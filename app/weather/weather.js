define(
  [
    'jquery',
    'react',
    'reactdom',
    'weather/description',
    'weather/location',
    'weather/status',
    'weather/refresh',
    'weather/error'
  ],
  function ($, React, ReactDOM, WeatherDescription, WeatherLocation,
    WeatherStatus, WeatherRefresh, WeatherError) {

    'use strict';

    // a reference to react's createElement method
    var CREATE_ELEMENT = React.createElement,
      // the browser's GeoLocation api
      GEOLOCATION = navigator.geolocation,
      // the list of error messages for when the GeoLocation was not found
      GEOLOCATION_ERRORS = {
        // When the user denies permission
        1 : 'Location request denied.',
        // When the position information is not available
        2 : 'Cannot find your location.',
        // When the request for the current position times out
        3 : 'Location aquisition timed out.',
        // When any other unknow error occurs
        4 : 'An unknown error occurred.'
      };

    /**
     * A method to wrap private methods and return the react class
     */
    return function () {
      // a reference to the new weather class to be created
      var self,
        /**
         * Adds an error to the current state of the class
         *
         * @method addError
         * @param {String} aErrorMessage A short description of the error
         */
        addError = function (aErrorMessage) {
          // set a flag that there is an error and add the message
          self.setState({error : true, errorMessage : aErrorMessage});
        },
        /**
         * Removes an error
         *
         * @method removeError
         */
        removeError = function () {
          // remove the error flag and message
          self.setState({error : false, errorMessage : ''});
        },
        /**
         * Adds a status to the weather class
         *
         * @method addStatus
         * @param {String} aStatus The status string to display
         */
        addStatus = function (aStatus) {
          // set the status on the state
          self.setState({statusText : aStatus, started : false});
        },
        /**
         * Gets the weather for the current location that was found
         *
         * @method getLocationWeather
         * @param {Object} aPosition The current geographical position
         */
        getLocationWeather = function (aPosition) {
          // the current coordinates
          var coordinates = aPosition.coords;
          // add status text showing we are fetching location weather
          addStatus('Fetching location weather.');
          // do an ajax call to the OpenWeather API
          $.get({
            // the url of the API call with the coordinates
            url : 'http://api.openweathermap.org/data/2.5/weather',
            // the list of data to send along with the url
            data : {
              // send the latitude to the url
              lat : coordinates.latitude,
              // send the longitude to the url
              lon : coordinates.longitude,
              // send the API key to the url
              APPID : '6dbcc0232b3a7e757418259ea6bf44e8',
              // use celcius, c'mon
              units : 'metric'
            },
            // when the request was successful
            success : showLocationWeather,
            // when there was an error with the request
            error : showGetLocationError
          });
        },
        /**
         * Shows an error when the user/device's location could not be found
         *
         * @method showLocationError
         * @param {Object} aError The error response containing information on
         *   the error that had occurred
         */
        showLocationError = function (aError) {
          // display a message based on the error code that was returned
          addError(GEOLOCATION_ERRORS[aError.code]);
        },
        /**
         * Shows an error when the API for location weather could not be reached
         *
         * @method showGetLocationError
         */
        showGetLocationError = function () {
          // add an error showing we could not fetch location information
          addError('Could not get location weather.');
        },
        /**
         * Shows the weather for a location once the weather was fetched from
         * the OpenWeather API
         *
         * @method showLocationWeather
         * @param {Object} aWeather The current weather conditions and location
         */
        showLocationWeather = function (aWeather) {
          // remove the error if there was one
          removeError();
          // set the temperature, short name and location name on the state and
          // indicate that weather display has started
          self.setState({
            // the weather display has started
            started : true,
            // the current temperature
            temperature : parseInt(aWeather.main.temp, 10),
            // the name of the weather
            shortName : aWeather.weather[0].main,
            // the location's name
            locationName : aWeather.name
          });
          // update the document title to also show the current weather
          document.title = parseInt(aWeather.main.temp, 10) + '\u02da ' +
            aWeather.name + ' - Warwick weather watchers.';
        },
        /**
         * Gets the user's current geo location using the browser's location api
         *
         * @method getGeoLocation
         */
        getGeoLocation = function () {
          // set a status showing the location is being fetched
          addStatus('Waiting for location.');
          // get the current location of the user/device
          GEOLOCATION.getCurrentPosition(getLocationWeather, showLocationError);
        },
        /**
         * Checks if the user has GeoLocation enabled on the device and adds an
         * error if it is not supported
         *
         * @method testGeoLocation
         */
        testGeoLocation = function () {
          // see if the user has a GeoLocation enabled device
          if (GEOLOCATION) {
            // get the user/device's GeoLocation
            getGeoLocation();
          }
          else {
            // add an error about the device geolocation not being enabled
            addError('No GeoLocation available.');
          }
        },
        /**
         * Create the description sub-element
         *
         * @method createDescription
         * @return {ReactElement} The react element that was created
         */
        createDescription = function () {
          // the element's current state
          var state = self.state;
          // create and return a description sub-element
          return createSub(state.started && !state.error, WeatherDescription,
            {shortName : state.shortName, temperature : state.temperature});
        },
        /**
         * Create the location sub-element
         *
         * @method createLocation
         * @return {ReactElement} The react element that was created
         */
        createLocation = function () {
          // the element's current state
          var state = self.state;
          // create and return a location sub-element
          return createSub(state.started && !state.error, WeatherLocation,
            {locationName : state.locationName});
        },
        /**
         * Create the status sub-element
         *
         * @method createStatus
         * @return {ReactElement} The react element that was created
         */
        createStatus = function () {
          // the element's current state
          var state = self.state;
          // create and return a status sub element
          return createSub(!state.started && !state.error, WeatherStatus,
            {statusText : state.statusText});
        },
        /**
         * Create the refresh sub-element
         *
         * @method createRefresh
         * @return {ReactElement} The react element that was created
         */
        createRefresh = function () {
          // the element's current state
          var state = self.state;
          // create a refresh sub-element and return it
          return createSub(state.started || state.error, WeatherRefresh,
            {refresh : testGeoLocation});
        },
        /**
         * Create the error sub-element
         *
         * @method createError
         * @return {ReactElement} The react element that was created
         */
        createError = function () {
          // the element's current state
          var state = self.state;
          // create an error sub-element and return it
          return createSub(state.error, WeatherError, {errorMessage : state
            .errorMessage});
        },
        /**
         * Returns a sub-element to attach to this element
         *
         * @method createSub
         * @param {Boolean} aCheck The check to perform whether to show the
         *   element
         * @param {ReactClass} aClass The react class to create an element from
         * @param {Object} aOptions The list of options to send to the element
         * @return {ReactElement} The react element that was created
         */
        createSub = function (aCheck, aClass, aOptions) {
          // return the element if the check passed otherwise return null
          return !aCheck ? null : CREATE_ELEMENT(aClass(), aOptions || {}, null);
        },
        /**
         * The weather react class that will house the weather display
         */
        Weather = React.createClass({
          // the html tag that will identify this class
          displayName : 'Weather',
          /**
           * Sets the initial state of the class
           *
           * @method getInitialState
           * @return {Object} An object containing the initial state of the
           *   weather class
           */
          getInitialState : function () {
            // return the initial state of the class
            return {
              // we are still waiting for the location to be found or not
              started : false,
              // the actual temperature
              temperature : 0,
              // the short name of the current weather conditions
              shortName : '',
              // the location name
              locationName : '',
              // the current status of the element
              statusText : '',
              // whether an error had occurred
              error : false,
              // the error message to display
              errorMessage : ''
            };
          },
          /**
           * When the component is about to be rendered and added to the DOM
           *
           * @method componentWillMount
           */
          componentWillMount : function () {
            // set a reference to the weather class
            self = this;
          },
          /**
           * When the element has been attached to the DOM we can begin our
           * journey
           *
           * @method componentDidMount
           */
          componentDidMount : function () {
            // test whether this device is GeoLocation enabled
            testGeoLocation();
          },
          /**
           * Renders the weather element
           *
           * @method render
           */
          render : function render() {
            // the list of properties for this element
            var properties = this.props,
              // the current state of the element
              state = this.state,
              // whether there is an error state
              error = state.error,
              // whether we are busy waiting for the location
              started = state.started && !error;
            // create a div that will house the weather display
            return CREATE_ELEMENT('div', null,
              // add the refresh link to the element
              createRefresh(),
              // the description of the current conditions
              createDescription(),
              // a span element that will show the name of the current location
              createLocation(),
              // a loading text element to show that the page is loading
              createStatus(),
              // an error element to show an error if there is one
              createError()
            );
          }
        });
      // return the weather react class
      return Weather;
    };
  }
);