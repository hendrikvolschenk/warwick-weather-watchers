define(
  [
    'react'
  ],
  function (React) {

    'use strict';

    /**
     * A method to wrap private methods and return the react class
     */
    return function () {
      /**
       * The weather status react class that will house the status display
       */
      var WeatherStatus = React.createClass({
        // the html tag that will identify this class
        displayName : 'WeatherStatus',
        /**
         * Renders the weather status element
         *
         * @method render
         */
        render : function render() {
          // a heading1 element that will display the short name of the
          // current weather conditions
          return React.createElement('p', null, this.props.statusText);
        }
      });
      // return the weather status react class
      return WeatherStatus;
    };
  }
);