define(
  [
    'react'
  ],
  function (React) {

    'use strict';

    /**
     * A method to wrap private methods and return the react class
     */
    return function () {
      /**
       * The weather error react class that will house the error display
       */
      var WeatherError = React.createClass({
        // the html tag that will identify this class
        displayName : 'WeatherError',
        /**
         * Renders the weather error element
         *
         * @method render
         */
        render : function render() {
          // a heading1 element that will display the short name of the
          // current weather conditions
          return React.createElement('p', {className : 'text-danger'},
            this.props.errorMessage);
        }
      });
      // return the weather error react class
      return WeatherError;
    };
  }
);