define(
  [
    'react'
  ],
  function (React) {

    'use strict';

    // a reference to react's createElement method
    var CREATE_ELEMENT = React.createElement;

    /**
     * An anonymous function to wrap private methods and return the react class
     */
    return function () {
      /**
       * A method to wrap private methods and return the react class
       */
      var WeatherDescription = React.createClass({
        // the html tag that will identify this class
        displayName : 'WeatherDescription',
        /**
         * Renders the weather description element
         *
         * @method render
         */
        render : function render() {
          // the list of properties
          var properties = this.props;
          // a heading1 element that will display the short name of the
          // current weather conditions
          return CREATE_ELEMENT('h1', null, properties.temperature, '\u02da',
            // a span to contain the sort name of the temperature
            CREATE_ELEMENT('span', null, properties.shortName)
          );
        }
      });
      // return the weather description react class
      return WeatherDescription;
    };
  }
);