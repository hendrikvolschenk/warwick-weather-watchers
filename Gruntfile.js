// Weather Gruntfile
module.exports = function (grunt) {

  // Project configuration
  grunt.initConfig({
    pkg: grunt.file.readJSON('package.json'),
    jshint: {
      js: ['app/**/*.js']
    },
    less: {
      development: {
        files: {
          'assets/css/main.css': 'assets/less/app.less'
        }
      }
    },
    watch: {
      js: {
        files: ['app/**/*.js'],
        tasks: ['jshint']
      },
      css: {
        files: ['assets/**/*.less'],
        tasks: ['less']
      }
    },
  });

  // Load the plugin that provides the "watch" task.
  grunt.loadNpmTasks('grunt-contrib-watch');
  // Load the plugin that provides the "jshint" task.
  grunt.loadNpmTasks('grunt-contrib-jshint');
  // Load the plugin that provides the "less" task.
  grunt.loadNpmTasks('grunt-contrib-less');
  // Default task(s).
  grunt.registerTask('default', ['less', 'watch']);
};