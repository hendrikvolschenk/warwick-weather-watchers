({
  // The base directory of the application
  appDir : './',
  // a path (relative to appDir) of where to find files
  baseUrl: "./app",
  // the output directory
  dir : './build',
  // the path to the main requirejs configuration file
  mainConfigFile: './app/main.js',
  // a list of modules to optimize
  modules : [
    {
      name : 'main'
    }
  ],
  // makes all text includes inline for more optimization
  inlineText: true,
  // a regex to run to exclude files to copy over to 'dir'
  fileExclusionRegExp : /^(requirejs\.build\.js|Gruntfile\.js|package\.json|server\-tools\.js|server\.js|build|node_modules|less|\.DS_STORE|build.js|npm-debug\.log|\.gitignore|README.md)$/,
  // how to optimize the javascript files
  optimize : 'uglify',
  // whether to optimize css files
  optimizeCss: 'standard',
  // whether to add 'use strict' for js files
  useStrict : true,
  // remove concatenated files from the output directory
  removeCombined : true,
  // whether to build the build.txt file when done with the build
  writeBuildTxt: false
});